from flask import Flask, render_template, request, redirect
import os
from flaskext.mysql import MySQL
import pymysql
from collections import defaultdict

app = Flask(__name__)


@app.route('/ovelse')
def ovelse():
    return render_template('ovelse.html')





DB_NAME = os.environ['DB_NAME']
DB_PASSWORD = os.environ['DB_PASSWORD']
DB_SERVER = os.environ['DB_SERVER']
DB_USER = os.environ['DB_USER']
try:
    DB_PORT = os.environ['DB_PORT']
    app.config['MYSQL_DATABASE_PORT'] = int(DB_PORT)
except:
    pass






app.config['MYSQL_DATABASE_USER'] = DB_USER
app.config['MYSQL_DATABASE_PASSWORD'] = DB_PASSWORD
app.config['MYSQL_DATABASE_DB'] = DB_NAME
app.config['MYSQL_DATABASE_HOST'] = DB_SERVER


mysql = MySQL()
mysql.init_app(app)



@app.route('/', methods=['GET', 'POST'])
def main():
    if request.method == "POST":
        return 'Hello, World!'
    if request.method == "GET":
        conn = mysql.connect()
        cursor = conn.cursor(pymysql.cursors.DictCursor)
        cursor.execute("SELECT * FROM Treningsøkt")
        rows = cursor.fetchall()
        return render_template('index.html')



@app.route('/apparat', methods=['GET', 'POST'])
def apparat():
    if request.method == "POST":
        _navn = request.form['apparat_navn']
        _beskrivelse = request.form['apparat_beskrivelse']
        data = (_navn, _beskrivelse)
        sql = "INSERT INTO Apparat (navn, beskrivelse) VALUES (%s, %s)"
        conn = mysql.connect()
        cursor = conn.cursor()
        cursor.execute(sql, data)
        conn.commit()
        return render_template('apparat.html')
    if request.method == "GET":
        return render_template('apparat.html')


@app.route('/ovelse/apparat', methods=['GET', 'POST'])
def ovelseApparat():
    if request.method == "POST":
        ##
        _navn = request.form['ovelse_navn']
        _apparatID = request.form['ovelse_apparatID']
        sql = "INSERT INTO Apparatøvelse (navn, apparatID) VALUES (%s, %s)"
        conn = mysql.connect()
        cursor = conn.cursor()
        data = (_navn, _apparatID)
        cursor.execute(sql, data)
        conn.commit()

    conn = mysql.connect()
    cursor = conn.cursor(pymysql.cursors.DictCursor)
    fetchsql = "SELECT * FROM Apparat"
    cursor.execute(fetchsql)
    result = cursor.fetchall()
    return render_template('ovelse_apparat.html', apparater=result)


@app.route('/ovelse/fri', methods=['GET', 'POST'])
def ovelseFri():
    if request.method == "POST":
        _navn = request.form['ovelse_navn']
        _repetisjonstype = request.form['ovelse_repetisjonstype']
        _beskrivelse = request.form['ovelse_beskrivelse']
        data = (_navn, _repetisjonstype, _beskrivelse)
        sql = "INSERT INTO Friøvelse (navn, repetisjonstype, beskrivelse) VALUES (%s, %s, %s)"
        conn = mysql.connect()
        cursor = conn.cursor()
        cursor.execute(sql, data)
        conn.commit()
        return render_template('ovelse_fri.html')
    if request.method == "GET":

        return render_template('ovelse_fri.html')



@app.route('/treningsokt', methods=['GET', 'POST'])
def treningsokt():
    if request.method == "POST":
        _dato = request.form['treningsokt_dato']
        _tidspunkt = request.form['treningsokt_tidspunkt']
        _varighet = request.form['treningsokt_varighet']
        _personligForm= request.form['treningsokt_personligForm']
        _senterID= request.form['treningsokt_senterID']
        data = (_dato, _tidspunkt, _varighet, _personligForm, _senterID)
        sql = "INSERT INTO Treningsøkt (dato, tidspunkt, varighet, personligForm, senterID) VALUES (%s, %s, %s, %s, %s)"
        conn = mysql.connect()
        cursor = conn.cursor()
        cursor.execute(sql, data)

        _formal = request.form['treningsokt_notat']
        if _formal != "":
            notatSQL = "INSERT INTO Notat (Formål, øktID) VALUES (%s, %s)"
            notatData = (_formal, cursor.lastrowid)
            cursor.execute(notatSQL, notatData)
        conn.commit()

    conn = mysql.connect()
    cursor = conn.cursor(pymysql.cursors.DictCursor)
    fetchsql = "SELECT * FROM Treningssenter "
    cursor.execute(fetchsql)
    result = cursor.fetchall()
    return render_template('treningsokt.html', sentere=result)



@app.route('/treningsokt/registrere', methods=['GET', 'POST'])
def treningsoktRegistrere():
    if request.method == "POST":
        _type = request.form['treningsokt_type']
        _treningsokt = request.form['treningsokt']

        uhtoSQL = "INSERT INTO UtførelseHørerTilØkt(øktID) VALUES (%s)"
        conn = mysql.connect()
        cursor = conn.cursor(pymysql.cursors.DictCursor)
        cursor.execute(uhtoSQL, _treningsokt)
        _utforelsesID = cursor.lastrowid
        _prestasjon = request.form['treningsokt_prestasjon']
        _sett = request.form['treningsokt_sett']
        _repetisjoner = request.form['treningsokt_repetisjoner']
        if _type == "fri":
            sql = "INSERT INTO FriUtførelse (friØvelseID, utførelsesØktID, prestasjon, sett, repetisjoner) VALUES (%s, %s, %s, %s, %s)"
            _friovelseID = request.form['treningsokt_friØvelseID']
            friData = (_friovelseID, _utforelsesID, _prestasjon, _sett, _repetisjoner)
            cursor.execute(sql, friData)
        else:
            sql = "INSERT INTO ApparatUtførelse (apparatØvelseID, utførelsesØktID, prestasjon, sett, repetisjoner, kilo) VALUES (%s, %s, %s, %s, %s, %s)"
            _apparatovelseID= request.form['treningsokt_apparatØvelseID']
            _kilo = request.form['treningsokt_kilo']
            appData= (_apparatovelseID, _utforelsesID, _prestasjon, _sett, _repetisjoner, _kilo)
            cursor.execute(sql, appData)
        conn.commit()


    conn = mysql.connect()
    cursor = conn.cursor(pymysql.cursors.DictCursor)
    fetchsql = "SELECT * FROM Treningsøkt"
    cursor.execute(fetchsql)
    treningsokter = cursor.fetchall()


    fetchsql = "SELECT * FROM Apparatøvelse"
    cursor.execute(fetchsql)
    apparatovelser = cursor.fetchall()

    fetchsql = "SELECT * FROM Friøvelse"
    cursor.execute(fetchsql)
    friøvelser = cursor.fetchall()
    return render_template('treningsokt_registrere.html', treningsokter=treningsokter, apparatøvelser=apparatovelser, friøvelser=friøvelser)



@app.route('/info', methods=['GET', 'POST'])
def info():
    #info_antall
    if request.method == "POST":
        sql = "SELECT T.dato, T.tidspunkt, N.formål, FØ.navn, FU.prestasjon, FU.sett,  FU.repetisjoner, FØ.repetisjonstype, T.ID FROM (SELECT * FROM Treningsøkt ORDER BY ID DESC LIMIT %s) AS T INNER JOIN UtførelseHørerTilØkt AS UHT ON (UHT.øktID = T.ID) INNER JOIN FriUtførelse AS FU ON (FU.utførelsesØktID = UHT.ID) INNER JOIN Friøvelse as FØ ON (FØ.ID = FU.friØvelseID) INNER JOIN Notat AS N ON (N.øktID = T.ID) ORDER BY ID DESC"
        conn = mysql.connect()
        cursor = conn.cursor(pymysql.cursors.DictCursor)
        data = int(request.form['info_antall'])
        cursor.execute(sql, data)
        friovelser = cursor.fetchall()


        sql = "SELECT T.dato, T.ID, T.tidspunkt, AØ.navn, AU.prestasjon, AU.sett, AU.repetisjoner, AU.prestasjon, AU.kilo, N.formål FROM (SELECT * FROM Treningsøkt ORDER BY ID DESC LIMIT %s) AS T INNER JOIN UtførelseHørerTilØkt AS UHT ON (UHT.øktID = T.ID) INNER JOIN ApparatUtførelse AS AU ON (AU.utførelsesØktID = UHT.ID) INNER JOIN Apparatøvelse as AØ ON (AØ.ID = AU.apparatØvelseID) INNER JOIN Notat AS N ON (N.øktID = T.ID) ORDER BY ID DESC"
        cursor.execute(sql, data)
        apparatøvelser = cursor.fetchall()

        #Convert to dictionary of treningsøkter
        treningsokter = defaultdict(list)
        for ø in apparatøvelser:
            ø['øvelsestype']="apparat"
            treningsokter[ø['ID']].append(ø)
        for ø in friovelser:
            ø['øvelsestype']="fri"
            treningsokter[ø['ID']].append(ø)
        return render_template('info.html', treningsokter=sorted(treningsokter.items(), reverse=True))
        # return render_template('info.html', treningsokter=treningsokter.items().sort(key = lambda x:x[0], reverse=True))


    return render_template('info.html')


@app.route('/ovelsesgruppe', methods=['GET', 'POST'])
def ovelsesgruppe():
    conn = mysql.connect()
    cursor = conn.cursor()
    if request.method == "POST":
        sql = "INSERT INTO Øvelsesgruppe (navn) VALUES (%s)"
        _navn = request.form['ovelsesgruppe_navn']
        appData = (_navn)
        cursor.execute(sql, appData)
        conn.commit()

    return render_template('ovelsesgruppe.html')

@app.route('/ovelsesgruppe/registrere', methods=['GET', 'POST'])
def ovelsesgruppeRegistrere():
    if request.method == "POST":
        if request.form['ovelsesgruppe_type'] == "apparatovelse":
            sql = "INSERT INTO ApparatKategori (øvelseID, øvelsesGruppeID) VALUES (%s, %s)"
            _øvelsesGruppeID = request.form['ovelsesgruppe_øvelsesGruppeID']
            _øvelseID = request.form['ovelsesgruppe_øvelseID']
        else:
            sql = "INSERT INTO FriKategori (øvelseID, øvelsesGruppeID) VALUES (%s, %s)"
            _øvelsesGruppeID = request.form['ovelsesgruppe_øvelsesGruppeID']
            _øvelseID = request.form['ovelsesgruppe_øvelseID']
        appData = (_øvelseID, _øvelsesGruppeID)
        conn = mysql.connect()
        cursor = conn.cursor(pymysql.cursors.DictCursor)
        cursor.execute(sql, appData)
        conn.commit()

    #Fetch grupper & øvelser:
    conn = mysql.connect()
    cursor = conn.cursor(pymysql.cursors.DictCursor)
    fetchsql = "SELECT * FROM Øvelsesgruppe"
    cursor.execute(fetchsql)
    grupper = cursor.fetchall()

    fetchsql = "SELECT * FROM Apparatøvelse"
    cursor.execute(fetchsql)
    apparatovelser = cursor.fetchall()

    fetchsql = "SELECT * FROM Friøvelse"
    cursor.execute(fetchsql)
    friovelser = cursor.fetchall()

    #ingen begrensning på at bruker ikke kan opprette flere grupper med samme navn, men det er ok her.

    return render_template('ovelsesgruppe_registrere.html', grupper=grupper, apparatovelser=apparatovelser, friovelser=friovelser)

@app.route('/viseovelsesgruppe', methods=['GET', 'POST'])
def viseovelsesgruppe():
    if request.method == "POST":
        conn = mysql.connect()
        cursor = conn.cursor(pymysql.cursors.DictCursor)
        _øvelsesgruppenavn = request.form['ovelsesgruppe_øvelseGruppeNavn']
        data = (_øvelsesgruppenavn, _øvelsesgruppenavn)
        sql = "SELECT Ø.navn FROM (SELECT ID FROM Øvelsesgruppe WHERE navn = %s) AS ØID INNER JOIN FriKategori AS FKat ON (FKat.øvelsesGruppeID = ØID.ID) INNER JOIN Friøvelse AS Ø ON (Ø.ID = FKat.øvelseID) UNION SELECT Ø.navn FROM (SELECT ID FROM Øvelsesgruppe WHERE navn = %s) AS ØID INNER JOIN ApparatKategori AS AKat ON (AKat.øvelsesGruppeID = ØID.ID) INNER JOIN Apparatøvelse AS Ø ON (Ø.ID = AKat.øvelseID)"
        cursor.execute(sql, data)
        ovelser = cursor.fetchall()
        fetchsql = "SELECT * FROM Øvelsesgruppe"
        cursor.execute(fetchsql)
        grupper = cursor.fetchall()
        return render_template('viseovelsesgruppe.html', ovelser=ovelser, grupper=grupper, grupperequest=request.form['ovelsesgruppe_øvelseGruppeNavn'])

    conn = mysql.connect()
    cursor = conn.cursor(pymysql.cursors.DictCursor)
    fetchsql = "SELECT * FROM Øvelsesgruppe"
    cursor.execute(fetchsql)
    grupper = cursor.fetchall()

    return render_template('viseovelsesgruppe.html', grupper=grupper)



@app.route('/resultat/friovelse', methods=['GET', 'POST'])
def resultatFriovelse():
    #info_antall
    if request.method == "POST":
        _startdato = request.form['resultat_startdato']
        _sluttdato = request.form['resultat_sluttdato']
        sql = "SELECT Ø.navn, FU.prestasjon, FU.repetisjoner, FU.sett, Ø.repetisjonstype, Økt.dato FROM (SELECT ID, dato FROM Treningsøkt WHERE dato between %s and %s ) AS Økt INNER JOIN UtførelseHørerTilØkt AS UHT ON (UHT.øktID = Økt.ID) INNER JOIN FriUtførelse AS FU ON (FU.utførelsesØktID = UHT.ID) INNER JOIN Friøvelse AS Ø ON (FU.friØvelseID = Ø.ID) WHERE Ø.ID = %s"
        conn = mysql.connect()
        cursor = conn.cursor(pymysql.cursors.DictCursor)
        data = (_startdato, _sluttdato, int(request.form['resultat_id']))
        cursor.execute(sql, data)
        resultatlogg = cursor.fetchall()
        cursor = conn.cursor(pymysql.cursors.DictCursor)
        fetchsql = "SELECT * FROM Friøvelse"
        cursor.execute(fetchsql)
        friøvelser = cursor.fetchall()
        return render_template('resultat_friovelse.html', øvelser=friøvelser, resultat=resultatlogg)





    conn = mysql.connect()
    cursor = conn.cursor(pymysql.cursors.DictCursor)
    fetchsql = "SELECT * FROM Friøvelse"
    cursor.execute(fetchsql)
    friøvelser = cursor.fetchall()

    return render_template('resultat_friovelse.html', øvelser=friøvelser)


@app.route('/resultat/apparatovelse', methods=['GET', 'POST'])
def resultatApparat():
    #info_antall
    if request.method == "POST":
        _startdato = request.form['resultat_startdato']
        _sluttdato = request.form['resultat_sluttdato']

        sql = "SELECT AØ.navn, AU.prestasjon, AU.sett, AU.repetisjoner, AU.kilo, Økt.dato FROM (SELECT ID, dato FROM Treningsøkt WHERE dato between %s and %s ) AS Økt INNER JOIN UtførelseHørerTilØkt AS UHT ON (UHT.øktID = Økt.ID) INNER JOIN ApparatUtførelse AS AU ON (AU.utførelsesØktID = UHT.ID) INNER JOIN Apparatøvelse AS AØ ON (AU.apparatØvelseID = AØ.ID) WHERE AØ.ID = %s"
        conn = mysql.connect()
        cursor = conn.cursor(pymysql.cursors.DictCursor)
        data = (_startdato, _sluttdato, int(request.form['resultat_id']))
        cursor.execute(sql, data)
        resultatlogg = cursor.fetchall()
        cursor = conn.cursor(pymysql.cursors.DictCursor)
        fetchsql = "SELECT * FROM Apparatøvelse"
        cursor.execute(fetchsql)
        apparatøvelser = cursor.fetchall()
        return render_template('resultat_apparat.html', øvelser=apparatøvelser, resultat=resultatlogg)


    conn = mysql.connect()
    cursor = conn.cursor(pymysql.cursors.DictCursor)
    fetchsql = "SELECT * FROM Apparatøvelse"
    cursor.execute(fetchsql)
    apparatøvelser = cursor.fetchall()

    return render_template('resultat_apparat.html', øvelser=apparatøvelser)


@app.route('/utforelser', methods=['GET', 'POST'])
def utforelser():
    #info_antall
    if request.method == "POST":
        _id = int(request.form['utforelse_id'])

        sql = "SELECT Ø.Navn, FU.prestasjon, FU.sett, FU.repetisjoner FROM (SELECT ID FROM Treningsøkt WHERE ID=%s) AS Økt INNER JOIN UtførelseHørerTilØkt AS UHT ON (UHT.øktID = Økt.ID) INNER JOIN FriUtførelse AS FU ON (FU.utførelsesØktID = UHT.ID) INNER JOIN Friøvelse AS Ø ON (FU.friØvelseID = Ø.ID) UNION SELECT AØ.Navn, AU.prestasjon, AU.sett, AU.repetisjoner FROM (SELECT ID FROM Treningsøkt WHERE ID=%s ) AS Økt INNER JOIN UtførelseHørerTilØkt AS UHT ON (UHT.øktID = Økt.ID) INNER JOIN ApparatUtførelse AS AU ON (AU.utførelsesØktID = UHT.ID) INNER JOIN Apparatøvelse AS AØ ON (AU.apparatØvelseID = AØ.ID)"
        conn = mysql.connect()
        cursor = conn.cursor(pymysql.cursors.DictCursor)
        data = (_id, _id)
        cursor.execute(sql, data)
        resultatlogg = cursor.fetchall()
        cursor = conn.cursor(pymysql.cursors.DictCursor)
        fetchsql = "SELECT * FROM Treningsøkt"
        cursor.execute(fetchsql)
        treningsøkt = cursor.fetchall()
        return render_template('utforelser.html', treningsøkter=treningsøkt, resultat=resultatlogg)


    conn = mysql.connect()
    cursor = conn.cursor(pymysql.cursors.DictCursor)
    fetchsql = "SELECT * FROM Treningsøkt"
    cursor.execute(fetchsql)
    treningsøkt = cursor.fetchall()

    return render_template('utforelser.html', treningsøkter=treningsøkt)

if __name__ == "__main__":
    app.run(host='0.0.0.0')
